package JdbcDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * @Auther: HP
 * @Date: 2021/3/24 07:24
 * @Description:JDBC快速入门
 */
public class JdbcDemo1 {
    public static void main(String[] args) throws Exception {
        //1.导入驱动jar包
        //2.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //3.获取数据库连接对象
        Connection coon = DriverManager.getConnection("jdbc:mysql://localhost:3306/db2", "root", "root");
        //4.定义sql语句
        String sql = "update jdbc_work set salary = 500 where id = 1001";
        //5.获取执行sql的对象Statement
        Statement stmt = coon.createStatement();
        //6.执行sql
        int count = stmt.executeUpdate(sql);
        //7.处理结果
        System.out.println(count);
        //8.释放资源
        stmt.close();
        coon.close();
    }
}
